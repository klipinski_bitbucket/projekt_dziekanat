package pl.pwn.reaktor.dziekanat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Data
@AllArgsConstructor
@NoArgsConstructor

//@Embeddable
// jeśli bysmy chciali żeby klasa address była używana w kilku tabelach
public class Address {

    private String street;

    private String city;
}
