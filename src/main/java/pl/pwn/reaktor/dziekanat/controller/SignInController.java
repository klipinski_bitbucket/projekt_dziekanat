package pl.pwn.reaktor.dziekanat.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import pl.pwn.reaktor.dziekanat.model.RoleEnum;
import pl.pwn.reaktor.dziekanat.model.User;
import pl.pwn.reaktor.dziekanat.service.UserService;

public class SignInController {

    @FXML
    private Button btnSignIn;

    @FXML
    private TextField tfLogin;

    @FXML
    private TextField tfPassword;

    @FXML
    void signIn(MouseEvent event) {

        String login = tfLogin.getText();
        String password = tfPassword.getText();
        User user = new User(login, password, RoleEnum.ROLE_STUDENT, true);

        UserService userService = new UserService();
        long saveUser = userService.save(user);
        System.out.println("SignIn user id: " + saveUser);
    }

}
