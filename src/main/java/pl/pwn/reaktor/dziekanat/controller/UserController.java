package pl.pwn.reaktor.dziekanat.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pl.pwn.reaktor.dziekanat.DziekanatMain;
import pl.pwn.reaktor.dziekanat.model.utills.CurrentUser;

import java.io.IOException;

public class UserController {

    @FXML
    private Button btnUpdateData;

    @FXML
    private Label lbLogin;

    @FXML
    void UpdateDataEvent(MouseEvent event) throws IOException {
        Stage primaryStage = DziekanatMain.getPrimaryStage();

        Parent root = FXMLLoader.load(getClass().getResource("/view/updateDataView.fxml"));
        primaryStage.setTitle("Update user data view");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public void initialize() {
        lbLogin.setText(lbLogin.getText() + CurrentUser.getCurrentUser()
                                                       .getLogin());
    }

    public void surveyEvent(MouseEvent mouseEvent) throws IOException {

        Stage primaryStage = DziekanatMain.getPrimaryStage();

        Parent root = FXMLLoader.load(getClass().getResource("/view/surveyView.fxml"));
        primaryStage.setTitle("Servey view");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
