package pl.pwn.reaktor.dziekanat.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

public class SurveyController {

    @FXML
    private MenuItem mSaveToFile;

    @FXML
    private MenuItem mSaveToDatabase;

    @FXML
    private MenuItem mClear;

    @FXML
    private MenuItem mClose;

    @FXML
    private MenuItem mAbout;

    @FXML
    private TextField tfName;

    @FXML
    private TextField tfLastName;

    @FXML
    private TextField tfMail;

    @FXML
    private TextField tfPhone;

    @FXML
    private CheckBox cbJava;

    @FXML
    private CheckBox cbPython;

    @FXML
    private CheckBox cbOther;

    @FXML
    private TextField tfOther;

    @FXML
    private RadioButton rbBasic;

    @FXML
    private ToggleGroup g1;

    @FXML
    private RadioButton rbIntermediate;

    @FXML
    private RadioButton rbAdvanced;

    @FXML
    private Button btnPreview;

    @FXML
    private TextArea taPreview;

    @FXML
    private ComboBox<?> cmbCurses;

    @FXML
    void aboutAction(ActionEvent event) {
        Alert info = new Alert(Alert.AlertType.INFORMATION);
        info.setTitle("About");
        info.setHeaderText("Instruction");
        info.setContentText("Instruction for using the form ...");
        info.show();
    }

    @FXML
    void clearAction(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    void closeAction(ActionEvent event) {

    }

    @FXML
    void otherAction(MouseEvent event) {

    }

    @FXML
    void preview(MouseEvent event) {

    }

    @FXML
    void saveToDB(ActionEvent event) {

    }

    @FXML
    void saveToFileAction(ActionEvent event) {

    }

}
