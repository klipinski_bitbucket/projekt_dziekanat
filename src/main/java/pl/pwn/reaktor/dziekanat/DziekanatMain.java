package pl.pwn.reaktor.dziekanat;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.pwn.reaktor.dziekanat.utils.HibernateUtils;

public class DziekanatMain extends Application {

    private static Stage primaryStage;

    // metoda używana do uruchomienia projektu bez komilatora, tylko z wyeksportowanego jar'a
    // dotyczy tylko JavaFx
    @Override
    public void start(Stage primaryStage) throws Exception {

        setPrimaryStage(primaryStage);
        Parent root = FXMLLoader.load(getClass().getResource("/view/loginView.fxml"));
        primaryStage.setTitle("Logowanie");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    //metoda używana tylko w InteliJ/Eclipse do uruchomienia projektu, która uruchamia
    // docelowo metodę start
    public static void main(String[] args) {
        launch(args);
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    private static void setPrimaryStage(Stage primaryStage) {
        DziekanatMain.primaryStage = primaryStage;
    }

    @Override
    public void init() throws Exception {
        super.init();
        System.out.println("Metoda init - uruchamiana tylko raz przy starcie projektu");
        HibernateUtils.initSessionFactory();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("Metoda stop - uruchamiana tylko raz przy zamykaniu projektu");
        HibernateUtils.destroySessionFactory();
    }
}
