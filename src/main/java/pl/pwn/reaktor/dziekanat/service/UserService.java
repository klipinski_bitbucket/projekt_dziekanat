package pl.pwn.reaktor.dziekanat.service;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.pwn.reaktor.dziekanat.model.Address;
import pl.pwn.reaktor.dziekanat.model.Student;
import pl.pwn.reaktor.dziekanat.model.User;
import pl.pwn.reaktor.dziekanat.model.dto.StudentDTO;
import pl.pwn.reaktor.dziekanat.utils.HibernateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserService {

    public long save(User user) {

        Session session = HibernateUtils.getSessionFactory()
                                        .openSession();
        Transaction trx = session.beginTransaction();

        long id = (Long) session.save(user);
        trx.commit();
        session.close();
        return id;
    }

    public void update(User user) {

        Session session = HibernateUtils.getSessionFactory()
                                        .openSession();
        Transaction trx = session.beginTransaction();

        session.update(user);

        trx.commit();
        session.close();
    }

    public List<User> getAllAdmin() {
        Session session = HibernateUtils.getSessionFactory()
                                        .openSession();
        Transaction trx = session.beginTransaction();

        Query query = session.createQuery("Select u from User u where u.role = 'ROLE_ADMIN'");

        List<User> admins = query.list();

        trx.commit();
        session.close();
        return admins;
    }

    public List<StudentDTO> getAllStudent() {
        Session session = HibernateUtils.getSessionFactory()
                                        .openSession();
        Transaction trx = session.beginTransaction();

        String hql = "Select new pl.pwn.reaktor.dziekanat.model.dto.StudentDTO" +
                     "(s.id, u.login, u.active, s.firstName, s.lastName, s.address.street, s.address.city) " +
                     "from User u inner join u.student s where u.role = 'ROLE_STUDENT'";

        Query query = session.createQuery(hql);
        List<StudentDTO> students = query.list();

        trx.commit();
        session.close();
        return students;
    }

    public List<StudentDTO> getAllStudentManual() {
        Session session = HibernateUtils.getSessionFactory()
                                        .openSession();
        Transaction trx = session.beginTransaction();

        String hql = "Select u from User u where u.role = 'ROLE_STUDENT'";

        Query query = session.createQuery(hql);
        List<User> students = query.list();

        List<StudentDTO> studentDTOS = new ArrayList<>();
        for (User user : students) {
            StudentDTO studentDTO = new StudentDTO();

            studentDTO.setLogin(user.getLogin());
            studentDTO.setActive(user.isActive());

            Student student = user.getStudent();
            if (Objects.nonNull(student)) {

                studentDTO.setId(student.getId());

                studentDTO.setFirstName(student.getFirstName());
                studentDTO.setLastName(student.getLastName());

                Address address = student.getAddress();
                if (Objects.nonNull(address)) {
                    studentDTO.setStreet(address.getStreet());
                    studentDTO.setCity(address.getCity());
                }
            }
            studentDTOS.add(studentDTO);
        }

        trx.commit();
        session.close();
        return studentDTOS;
    }

}
